﻿namespace Entity.Enums
{
    public enum UserRoles
    {
        User,
        Moderator,
        Admin
    }
}
