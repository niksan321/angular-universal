import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
// модули
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app.routing';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material.module';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { CookieModule } from 'ngx-cookie';

// настройки
import { environment } from '../environments/environment';

// перехватчики
import { AuthRedirectInterceptor } from './interceptors/auth-redirect.interceptor';

// компоненты
import { AdminPageComponent } from './components/pages/admin-page/admin-page.component';
import { UserPageComponent } from './components/pages/user-page/user-page.component';
import { GuestPageComponent } from './components/pages/guest-page/guest-page.component';
import { AboutPageComponent } from './components/pages/about-page/about-page.component';
import { ModeratorPageComponent } from './components/pages/moderator-page/moderator-page.component';
import { AppComponent } from './components/app/app.component';
import { MenuComponent } from './components/menu/menu.component';
import { LoginComponent } from './components/login/login.component';
import { CommonLayoutComponent } from './components/common-layout/common-layout.component';
import { TestCardComponent } from './components/test-card/test-card.component';
import { CookieStorage } from './services/cookie.storage';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CommonLayoutComponent,
    AdminPageComponent,
    UserPageComponent,
    ModeratorPageComponent,
    GuestPageComponent,
    MenuComponent,
    AboutPageComponent,
    TestCardComponent
  ],
  imports: [
    CommonModule,
    NgtUniversalModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    NgHttpLoaderModule,
    CookieModule.forRoot(),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [`${environment.url.api.base}`],
        sendAccessToken: true
      }
    })
  ],
  providers: [{ provide: OAuthStorage, useClass: CookieStorage }, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthRedirectInterceptor,
    multi: true
  }],
})
export class AppModule { }
