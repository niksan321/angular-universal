import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from '../environments/environment';

export const OAuthPasswordFlowConfig: AuthConfig = {
    issuer: environment.url.identityServer.base,
    // redirectUri: window.location.origin,
    // postLogoutRedirectUri: window.location.origin,
    clientId: environment.auth.client.id,
    dummyClientSecret: environment.auth.client.secret,
    scope: environment.auth.client.scope,
    showDebugInformation: environment.auth.client.showDebugInformation,
    requireHttps: environment.auth.client.requareHttps,
    tokenEndpoint: environment.url.identityServer.token,
    oidc: false
};
