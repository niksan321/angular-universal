import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { default as JwtDecode } from 'jwt-decode';
import { OAuthService } from 'angular-oauth2-oidc';


@Injectable({ providedIn: 'root' })
export class RoleGuardService implements CanActivate, CanActivateChild {

  constructor(private router: Router, private oauthService: OAuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : boolean | Observable<boolean> | Promise<boolean> {
    return this.checkRole(route, state);
  }

  canActivateChild(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return this.checkRole(route, state);
  }

  checkRole(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const rootRoles = route.parent.data['roles'];
    const currentRoles = route.data['roles'];
    const routeRoles: string[] = currentRoles || rootRoles;
    const hasValidAccessToken = this.oauthService.hasValidAccessToken();

    if (hasValidAccessToken) {
      return this.checkAccess(routeRoles);
    }
    return this.refreshTokenAndChekAccess(routeRoles);
  }

  refreshTokenAndChekAccess(routeRoles: string[]): Observable<boolean> {
    return Observable.create(observer => {
      this.oauthService.refreshToken().then(
        token => {
          const hasValidAccessToken = this.oauthService.hasValidAccessToken();
          if (hasValidAccessToken) {
            const access = this.checkAccess(routeRoles);
            observer.next(access);
          } else {
            observer.next(false);
          }
          observer.complete();
        },
        err => {
          observer.next(false);
        }
      );
    });
  }

  checkAccess(routeRoles: string[]): boolean {
    const accessToken = this.oauthService.getAccessToken();
    const payload = JwtDecode(accessToken);
    if (routeRoles && payload && routeRoles.includes(payload.role)) {
      return true;
    }
    return false;
  }

}
