import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthEventEnum } from '../enums/o-auth-event.enum';
import { OAuthService, OAuthEvent } from 'angular-oauth2-oidc';
import { OAuthPasswordFlowConfig } from '../auth.config';
import { Roles } from '../enums';
import { default as JwtDecode } from 'jwt-decode';
import { ReplaySubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class OAuth {

    private _isLoggedIn = new ReplaySubject<boolean>(1);
    private _isUser = new ReplaySubject<boolean>(1);
    private _isModerator = new ReplaySubject<boolean>(1);
    private _isAdmin = new ReplaySubject<boolean>(1);

    constructor(private oauthService: OAuthService,
        private router: Router) { }

    public configurePasswordFlow() {
        this.oauthService.configure(OAuthPasswordFlowConfig);
        this.oauthService.events.subscribe(event => this.HandleEvent(event));
    }

    public isInRole(role: Roles): boolean {
        if (!this.oauthService.hasValidAccessToken()) {
            return false;
        }
        const accessToken = this.oauthService.getAccessToken();
        const payload = JwtDecode(accessToken);
        if (role === payload.role) {
            return true;
        }
        return false;
    }

    public isUser(): ReplaySubject<Boolean> {
        this._isUser.next(this.isInRole(Roles.user));
        return this._isUser;
    }

    public isModerator(): ReplaySubject<Boolean> {
        this._isModerator.next(this.isInRole(Roles.moderator));
        return this._isModerator;
    }

    public isAdmin(): ReplaySubject<Boolean> {
        this._isAdmin.next(this.isInRole(Roles.admin));
        return this._isAdmin;
    }

    public getClaim(claimName: string): string {
        const accessToken = this.oauthService.getAccessToken();
        const payload = JwtDecode(accessToken);
        return payload[claimName];
    }

    public isLoggedInAsync(): ReplaySubject<boolean> {
        this._isLoggedIn.next(this.oauthService.hasValidAccessToken());
        return this._isLoggedIn;
    }

    public login(login: string, password: string) {
        return this.oauthService.fetchTokenUsingPasswordFlow(login, password);
    }

    public logout() {
        this._isLoggedIn.next(false);
        this.redirectToHome();
        return this.oauthService.logOut();
    }

    public getName() {
        return this.oauthService.hasValidAccessToken() ? this.getClaim('name') : '';
    }

    private HandleEvent(event: OAuthEvent) {
        // for debug only
        // console.log(event);

        switch (event.type) {
            case OAuthEventEnum.token_refresh_error: {
                this.redirectToHome();
                break;
            }
            case OAuthEventEnum.token_expires: {
                this.oauthService.refreshToken();
                break;
            }
        }
    }

    private redirectToHome() {
        this.router.navigate(['/']);
    }
}
