import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonLayoutComponent } from './components/common-layout/common-layout.component';
import { RoleGuardService } from './services/role-guard.service';
import { AdminPageComponent } from './components/pages/admin-page/admin-page.component';
import { ModeratorPageComponent } from './components/pages/moderator-page/moderator-page.component';
import { Roles } from './enums';
import { UserPageComponent } from './components/pages/user-page/user-page.component';
import { GuestPageComponent } from './components/pages/guest-page/guest-page.component';
import { AboutPageComponent } from './components/pages/about-page/about-page.component';

const routes = <Routes>[
  { path: '', redirectTo: '/guest-page', pathMatch: 'full' },
  {
    path: '',
    component: CommonLayoutComponent,
    children: [
      { path: 'guest-page', component: GuestPageComponent },
      { path: 'about-page', component: AboutPageComponent }
    ]
  },
  {
    path: '',
    component: CommonLayoutComponent,
    canActivateChild: [RoleGuardService],
    data: { roles: [Roles.admin] },
    children: [
      { path: 'admin-page', component: AdminPageComponent },
    ]
  },
  {
    path: '',
    component: CommonLayoutComponent,
    canActivateChild: [RoleGuardService],
    data: { roles: [Roles.admin, Roles.moderator] },
    children: [
      { path: 'moderator-page', component: ModeratorPageComponent },
    ]
  },
  {
    path: '',
    component: CommonLayoutComponent,
    canActivateChild: [RoleGuardService],
    data: { roles: [Roles.user, Roles.admin, Roles.moderator] },
    children: [
      { path: 'user-page', component: UserPageComponent }
    ]
  },

  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // enableTracing: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
