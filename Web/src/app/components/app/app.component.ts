import { Component } from '@angular/core';
import { OAuth } from '../../services/oauth.service';
import { Spinkit } from 'ng-http-loader';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public spinkit = Spinkit;

  constructor(private oauthService: OAuth) {
    this.oauthService.configurePasswordFlow();
  }
}
