import { Component, OnInit } from '@angular/core';
import { Roles } from '../../enums';

@Component({
  selector: 'app-common-layout',
  templateUrl: './common-layout.component.html',
  styleUrls: ['./common-layout.component.scss']
})
export class CommonLayoutComponent implements OnInit {
  Roles: typeof Roles = Roles;
  data: any;

  constructor() { }

  ngOnInit() {
  }
}
