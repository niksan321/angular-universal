import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-test-card',
  templateUrl: './test-card.component.html',
  styleUrls: ['./test-card.component.scss']
})
export class TestCardComponent implements OnInit {
  @Input() title: string;
  data: string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  callGuestApi() {
    this.callApi(environment.url.api.user.guest);
  }

  callUserApi() {
    this.callApi(environment.url.api.user.user);
  }

  callModeratorApi() {
    this.callApi(environment.url.api.user.moderator);
  }

  callAdminApi() {
    this.callApi(environment.url.api.user.admin);
  }

  callclaimApi() {
    this.callApi(environment.url.api.user.claims);
  }

  callApi(url) {
    this.http.get(url)
      .subscribe(responce => {
        this.data = JSON.stringify(responce, null, 2);
      }, error => {
        this.data = JSON.stringify(error, null, 2);
      });
  }

}
