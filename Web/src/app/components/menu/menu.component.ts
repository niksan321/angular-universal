import { Component, OnInit } from '@angular/core';
import { OAuth } from '../../services/oauth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(public oauth: OAuth) { }

  ngOnInit() {
  }

}
