import { Component, OnInit } from '@angular/core';
import { OAuth } from '../../services/oauth.service';
import { LoginModel } from '../../models';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  username: FormControl;
  password: FormControl;

  model: {
    error: string,
    loginForm: FormGroup,
    userName: string
  };

  constructor(public oauth: OAuth, public snackBar: MatSnackBar) {
    this.model = {
      error: '',
      loginForm: this.getLoginForm(),
      userName: this.oauth.getName()
    };
  }

  getLoginForm(): FormGroup {
    this.username = new FormControl('', [Validators.required]);
    this.password = new FormControl('', [Validators.required]);

    return new FormGroup({
      username: this.username,
      password: this.password
    });
  }

  onSubmit(loginModel: LoginModel) {
    this.model.error = '';

    this.oauth.login(loginModel.username, loginModel.password)
      .then(() => {
        this.model.userName = this.oauth.getName();
      }, error => {
        console.log(error);
        this.model.error = error.message;
        this.snackBar.open(error.error.error_description);
      });
  }

}
