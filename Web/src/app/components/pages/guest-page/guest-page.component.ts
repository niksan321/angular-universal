import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-guest-page',
  templateUrl: './guest-page.component.html',
  styleUrls: ['./guest-page.component.scss']
})
export class GuestPageComponent implements OnInit {
  constructor(private meta: Meta, private title: Title) { }

  ngOnInit() {
    this.title.setTitle('Страница гостя');
    this.meta.addTags([{ name: 'description', content: 'Описание гостя' }]);
  }
}
