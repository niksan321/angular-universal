import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  constructor(private meta: Meta, private title: Title) { }

  ngOnInit() {
    this.title.setTitle('Страница пользователя');
    this.meta.addTags([{ name: 'description', content: 'Описание пользователя' }]);
  }
}
