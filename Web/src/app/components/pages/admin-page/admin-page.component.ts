import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {
  constructor(private meta: Meta, private title: Title) { }

  ngOnInit() {
    this.title.setTitle('Страница админа');
    this.meta.addTags([{ name: 'description', content: 'Описание админа' }]);
  }
}
