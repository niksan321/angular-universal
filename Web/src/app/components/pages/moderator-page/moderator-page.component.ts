import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-moderator-page',
  templateUrl: './moderator-page.component.html',
  styleUrls: ['./moderator-page.component.scss']
})
export class ModeratorPageComponent implements OnInit {
  constructor(private meta: Meta, private title: Title) { }

  ngOnInit() {
    this.title.setTitle('Страница модератора');
    this.meta.addTags([{ name: 'description', content: 'Описание модератора' }]);
  }
}
