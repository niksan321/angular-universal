// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const BASE_API_URL = 'http://localhost:51828/api';
const BASE_IDENTITY_SERVER_URL = 'https://localhost:44342';

export const environment = {
  production: false,
  url: {
    api: {
      base: `${BASE_API_URL}`,
      user: {
        guest: `${BASE_API_URL}/guest`,
        user: `${BASE_API_URL}/user`,
        moderator: `${BASE_API_URL}/moderator`,
        admin: `${BASE_API_URL}/admin`,
        claims: `${BASE_API_URL}/claims`,
        id: `${BASE_API_URL}/id`,
      }
    },
    identityServer: {
      base: `${BASE_IDENTITY_SERVER_URL}`,
      token: `${BASE_IDENTITY_SERVER_URL}/connect/token`
    }

  },
  auth: {
    client: {
      id: 'WebSpa',
      secret: 'Zzfe58Qgqk5FcAQPGPx5rGSSYtahWkD7BxvT2FL2SQyYa9TBAptTSTwHjY4edscc',
      scope: 'openid profile ApiV1 offline_access',
      showDebugInformation: true,
      requareHttps: false
    }
  }

};
