﻿using System;
using System.Collections.Generic;
using Common.Enums;
using Common.Extensions;
using IdentityServer4;
using IdentityServer4.Models;

namespace IdentityServer.Clients
{
    public class WebClient : Client
    {
        public WebClient()
        {
            ClientId = ClientIdName.WebSpa.ToString();
            ClientName = ClientIdName.WebSpa.GetDisplayName();

            ClientSecrets = new List<Secret>
            {
                new Secret("Zzfe58Qgqk5FcAQPGPx5rGSSYtahWkD7BxvT2FL2SQyYa9TBAptTSTwHjY4edscc".Sha256())
            };

            AllowedScopes = new List<String>
            {
                IdentityServerConstants.StandardScopes.OpenId,
                IdentityServerConstants.StandardScopes.Profile,
                IdentityServerConstants.StandardScopes.OfflineAccess,
                ResourceName.ApiV1.ToString()
            };

            AllowedGrantTypes = GrantTypes.ResourceOwnerPassword;
            AllowAccessTokensViaBrowser = true;
            AllowOfflineAccess = true;
            UpdateAccessTokenClaimsOnRefresh = true;
            RefreshTokenExpiration = TokenExpiration.Sliding;
            AlwaysIncludeUserClaimsInIdToken = true;

            AllowedCorsOrigins = new List<string> {"http://localhost:4200"};
            AccessTokenLifetime = 300;
            IdentityTokenLifetime = 300;
            SlidingRefreshTokenLifetime = 300;
        }
    }
}
