﻿namespace IdentityServer.Settings
{
    public class DatabaseSettings
    {
        public string ConnectionString { get; set; }
    }
}
