﻿using IdentityServer4.Models;

namespace IdentityServer.Models.Home
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}