﻿using System;
using System.Collections.Generic;
using Common.Enums;
using Common.Extensions;
using IdentityModel;
using IdentityServer.Clients;
using IdentityServer4.Models;

namespace IdentityServer.Config
{
    public class Config
    {
        public List<ApiResource> GetApiResources()
        {
            var apiResources = new List<ApiResource>
            {
                new ApiResource(ResourceName.ApiV1.ToString(), ResourceName.ApiV1.GetDisplayName())
                {
                    ApiSecrets = { new Secret("apisecret".Sha256()) },
                    UserClaims = { JwtClaimTypes.Role, JwtClaimTypes.Name, JwtClaimTypes.Email }
                }
            };
            return apiResources;
        }

        public List<Client> GetClients()
        {
            return new List<Client>
            {
                new WebClient()
            };
        }

        /// <summary>
        /// Ресурсы для OpenId
        /// </summary>
        public List<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }
    }
}
