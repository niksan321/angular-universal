﻿namespace Api.Settings
{
    public class IdentityServerSettings
    {
        public string AuthorityUrl { get; set; }
        public bool RequireHttpsMetadata { get; set; }
        public string DefaultScheme { get; set; }
    }
}