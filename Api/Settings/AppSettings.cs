﻿namespace Api.Settings
{
    public class AppSettings
    {
        public DatabaseSettings Database { get; set; }
        public IdentityServerSettings IdentityServer { get; set; }
        public UrlSettings Url { get; set; }
    }
}
