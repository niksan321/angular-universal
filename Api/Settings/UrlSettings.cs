﻿namespace Api.Settings
{
    public class UrlSettings
    {
        public string Web { get; set; }
        public string Api { get; set; }
    }
}
