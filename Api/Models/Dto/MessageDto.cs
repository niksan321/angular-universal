﻿namespace Api.Models.Dto
{
    public class MessageDto
    {
        public string Message { get; set; }
        public string UserName { get; set; }
    }
}
