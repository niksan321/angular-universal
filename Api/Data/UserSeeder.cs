﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Common.Extensions;
using Entity.Models;
using Entity.Enums;
using Microsoft.AspNetCore.Identity;
using IdentityModel;

namespace Api.Data
{
    public class UserSeeder
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserSeeder(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task SeedAsync()
        {
            await SeedUser("admin", "admin@mail.ru", "q123456", UserRoles.Admin);
            await SeedUser("user", "user@mail.ru", "q123456", UserRoles.User);
            await SeedUser("moderator", "moderator@mail.ru", "q123456", UserRoles.Moderator);
        }

        private async Task SeedUser(string name, string email, string password, UserRoles role)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null) return;

            user = await CreateUserAsync(name, email, password);
            await AddClaimsAsync(user, role);
        }

        private async Task AddClaimsAsync(ApplicationUser user, UserRoles role)
        {
            var result = await _userManager.AddClaimsAsync(user, new[]{
                new Claim(JwtClaimTypes.Subject, user.Id.ToString()),
                new Claim(JwtClaimTypes.Role, role.ToIntString())
            });

            if (!result.Succeeded)
            {
                throw new Exception(result.Errors.First().Description);
            }
        }

        private async Task<ApplicationUser> CreateUserAsync(string name, string email, string password)
        {
            var user = new ApplicationUser
            {
                UserName = name,
                Email = email,
                EmailConfirmed = true,
            };
            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                throw new Exception(result.Errors.First().Description);
            }

            return user;
        }
    }
}
