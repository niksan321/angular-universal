﻿using Api.Settings;
using Common.Enums;
using Entity;
using Entity.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var settings = _configuration.GetSection("Root").Get<AppSettings>();
            services.Configure<AppSettings>(_configuration.GetSection("Root"));

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials();
                    });
            });

            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(settings.Database.ConnectionString));

            services.AddIdentity<ApplicationUser, ApplicationRole>(cfg =>
                {
                    cfg.Password.RequireNonAlphanumeric = false;
                    cfg.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<AppDbContext>();

            services.AddAuthentication(opt =>
                {   // указывая только одну схему defaultScheme работать не будет, баг 
                    opt.DefaultAuthenticateScheme = settings.IdentityServer.DefaultScheme;
                    opt.DefaultChallengeScheme = settings.IdentityServer.DefaultScheme;
                    opt.DefaultForbidScheme = settings.IdentityServer.DefaultScheme;
                    opt.DefaultScheme = settings.IdentityServer.DefaultScheme;
                    opt.DefaultSignInScheme = settings.IdentityServer.DefaultScheme;
                    opt.DefaultSignOutScheme = settings.IdentityServer.DefaultScheme;
                })
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = settings.IdentityServer.AuthorityUrl;
                    options.RequireHttpsMetadata = settings.IdentityServer.RequireHttpsMetadata;
                    options.ApiName = ResourceName.ApiV1.ToString();
                    options.ApiSecret = "apisecret";
                });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowAll");
            app.UseAuthentication();
            app.UseMvcWithDefaultRoute();
        }
    }
}
