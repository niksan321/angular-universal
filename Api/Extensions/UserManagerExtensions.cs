﻿using System.Threading.Tasks;
using Entity.Models;
using Microsoft.AspNetCore.Identity;

namespace Api.Extensions
{
    public static class UserManagerExtensions
    {
        public static async Task<ApplicationUser> FindByIdAsync(this UserManager<ApplicationUser> userManager, long id)
        {
            return await userManager.FindByIdAsync(id.ToString());
        }
    }
}
