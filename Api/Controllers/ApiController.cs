﻿using System;
using System.Linq;
using Api.Models.Dto;
using Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("[controller]")]
    public class ApiController : Controller
    {
        [Route("Guest")]
        [HttpGet]
        public ActionResult<MessageDto> Guest()
        {
            return new MessageDto
            {
                Message = "Ответ сервера для гостя",
                UserName = User?.Identity?.Name
            };
        }

        [Route("User")]
        [Authorize(Roles = RoleConstants.UserModeratorAdmin)]
        [HttpGet]
        public ActionResult<MessageDto> GetForUser()
        {
            return new MessageDto
            {
                Message = "Ответ сервера для пользователя",
                UserName = User.Identity.Name
            };
        }

        [Route("Moderator")]
        [Authorize(Roles = RoleConstants.ModeratorAdmin)]
        [HttpGet]
        public ActionResult<MessageDto> Moderator()
        {
            return new MessageDto
            {
                Message = "Ответ сервера для модератора",
                UserName = User.Identity.Name
            };
        }

        [Route("Admin")]
        [Authorize(Roles = RoleConstants.Admin)]
        [HttpGet]
        public ActionResult<MessageDto> Admin()
        {
            return new MessageDto
            {
                Message = "Ответ сервера для админа",
                UserName = User.Identity.Name
            };
        }

        [Route("Claims")]
        [Authorize]
        [HttpGet]
        public IActionResult Claims()
        {
            return new JsonResult(from c in User.Claims select new {c.Type, c.Value});
        }
    }
}
