﻿using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        public string Index()
        {
            return "It's ALIVE";
        }
    }
}
