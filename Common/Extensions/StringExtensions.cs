﻿namespace Common.Extensions
{
    public static class StringExtensions
    {
        public static string ReplaceSlashesToBackwards(this string s)
        {
            return s.Replace('\\', '/');
        }
    }
}
