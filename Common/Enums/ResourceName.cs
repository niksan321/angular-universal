﻿using System.ComponentModel.DataAnnotations;

namespace Common.Enums
{
    public enum ResourceName
    {
        [Display(Name = "Api resourses v1")]
        ApiV1
    }
}