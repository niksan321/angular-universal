﻿using System.ComponentModel.DataAnnotations;

namespace Common.Enums
{
    public enum ClientIdName
    {
        [Display(Name ="Web single page application")]
        WebSpa
    }
}
