﻿namespace Common.Constants
{
    public class RoleConstants
    {
        public const string User = "0";
        public const string Moderator = "1";
        public const string Admin = "2";
        public const string UserAndModerator = "0,1";
        public const string UserAndAdmin = "0,2";
        public const string UserModeratorAdmin = "0,1,2";
        public const string ModeratorAdmin = "1,2";
    }
}
